package ru.kev.socket;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Класс, в котором запукается сервер.
 * Сервер ожидает подключения клиента.
 * Сервер оповещает  о подключении  клиента.
 * Выводит сообщения, которые прислал клиент и сервер отправляет его обратно.
 * Если пользователь отключается от сервера, то сервер оповещает об этом.
 *
 * @author Karnauhov Evnegiy 15ИТ18
 */

public class Server {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8000);
        System.out.println("Сервер ожидает клиента...");

        try(Socket clientSocket = serverSocket.accept();
            InputStream inputStream = clientSocket.getInputStream();
            OutputStream outputStream = clientSocket.getOutputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream))){

            System.out.println("Новое соединение: " + clientSocket.getInetAddress().toString());

            String string;
            while ((string = reader.readLine())!=null){
                writer.write(string);
                System.out.println("Клиент прислал на сервер:" + string);
                System.out.println("Сервер ответил: " + string);
                if(string.equals("gg")){
                    break;
                }
                outputStream.flush();
            }
            System.out.println("Клиент отключился от сервера!");
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}