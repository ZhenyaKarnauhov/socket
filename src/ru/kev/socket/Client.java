package ru.kev.socket;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Класс, в котором запускается клиент.
 * Пользователь вводит сообщение, которое отправится на сервер.
 * Сервер отправляет ответ в виде того же сообщения.
 * При получении спец.слова , сервер завершает работу.
 *
 * @author Karnauhov Evgeniy 15ИТ18
 */

public class Client {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        try (Socket socket = new Socket("localhost", 8000);
             InputStream inputStream = socket.getInputStream();
             OutputStream outputStream = socket.getOutputStream();
             BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
             BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream))) {

            while (true){
                String string;
                System.out.println("Введите сообщение для отправки на сервер: ");
                string = scan.nextLine();
                writer.write(string);
                System.out.println("отправил клиент: " + string);
                System.out.println("прислал сервер: " + string);
                if (string.equals("gg")){
                    break;
                }
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }
}